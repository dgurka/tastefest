Option Compare Database

Private Sub ADD_NEW_RECORD_Click()
On Error GoTo Err_ADD_NEW_RECORD_Click


    DoCmd.GoToRecord , , acNewRec

Exit_ADD_NEW_RECORD_Click:
    Exit Sub

Err_ADD_NEW_RECORD_Click:
    MsgBox Err.Description
    Resume Exit_ADD_NEW_RECORD_Click
    
End Sub

Private Sub Emerald_Click()
    If Me.Emerald = True Then
        Dim RecordSt As Recordset
        Dim dBase As Database
        Dim stringSQL As String
        
        Set dBase = CurrentDb
        
        stringSQL = "SELECT [Cost] FROM [Pricing] WHERE [Item] = 'Emerald'"
        Set RecordSt = dBase.OpenRecordset(stringSQL)
        RecordSt.MoveFirst
        Me.Emerald_Total = RecordSt.Fields("Cost").Value
    Else
        Me.Emerald_Total = 0
    End If
End Sub

Private Sub Diamond_Click()
    If Me.Diamond = True Then
        Dim RecordSt As Recordset
        Dim dBase As Database
        Dim stringSQL As String
        
        Set dBase = CurrentDb
        
        stringSQL = "SELECT [Cost] FROM [Pricing] WHERE [Item] = 'Diamond'"
        Set RecordSt = dBase.OpenRecordset(stringSQL)
        RecordSt.MoveFirst
        Me.Diamond_Total = RecordSt.Fields("Cost").Value
    Else
        Me.Diamond_Total = 0
    End If
End Sub

Private Sub Ruby_Click()
    If Me.Ruby = True Then
        Dim RecordSt As Recordset
        Dim dBase As Database
        Dim stringSQL As String
        
        Set dBase = CurrentDb
        
        stringSQL = "SELECT [Cost] FROM [Pricing] WHERE [Item] = 'Ruby'"
        Set RecordSt = dBase.OpenRecordset(stringSQL)
        RecordSt.MoveFirst
        Me.Ruby_Total = RecordSt.Fields("Cost").Value
    Else
        Me.Ruby_Total = 0
    End If
End Sub

Private Sub Platinum_Click()
    If Me.Platinum = True Then
        Dim RecordSt As Recordset
        Dim dBase As Database
        Dim stringSQL As String
        
        Set dBase = CurrentDb
        
        stringSQL = "SELECT [Cost] FROM [Pricing] WHERE [Item] = 'Platinum'"
        Set RecordSt = dBase.OpenRecordset(stringSQL)
        RecordSt.MoveFirst
        Me.Platinum_Total = RecordSt.Fields("Cost").Value
    Else
        Me.Platinum_Total = 0
    End If
End Sub

Private Sub Gold_Click()
    If Me.Gold = True Then
        Dim RecordSt As Recordset
        Dim dBase As Database
        Dim stringSQL As String
        
        Set dBase = CurrentDb
        
        stringSQL = "SELECT [Cost] FROM [Pricing] WHERE [Item] = 'Gold'"
        Set RecordSt = dBase.OpenRecordset(stringSQL)
        RecordSt.MoveFirst
        Me.Gold_Total = RecordSt.Fields("Cost").Value
    Else
        Me.Gold_Total = 0
    End If
End Sub

Private Sub Silver_Click()
    If Me.Silver = True Then
        Dim RecordSt As Recordset
        Dim dBase As Database
        Dim stringSQL As String
        
        Set dBase = CurrentDb
        
        stringSQL = "SELECT [Cost] FROM [Pricing] WHERE [Item] = 'Silver'"
        Set RecordSt = dBase.OpenRecordset(stringSQL)
        RecordSt.MoveFirst
        Me.Silver_Total = RecordSt.Fields("Cost").Value
    Else
        Me.Silver_Total = 0
    End If
End Sub
