<?php

require_once(BASE_DIR . "bootstrap.php");

$OpId = get("OpId");
$StatusCode = get("StatusCode");

$conn = Db::GetNewConnection();

$OpId = Db::EscapeString($OpId, $conn);
$StatusCode = (int)$StatusCode;

Db::ExecuteNonQuery("UPDATE c_transaction SET status = $StatusCode WHERE opid = '$OpId'", $conn);

Db::CloseConnection($conn);

$context = getDefaultContext();

if($StatusCode == "100")
{
	echo $twig->render('intuit_landing_success.html', $context);
}
else
{
	echo $twig->render('intuit_landing_failure.html', $context);
}
