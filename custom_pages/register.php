<?php

require_once(BASE_DIR . "bootstrap.php");

function addItem($item, $quantity, $cost)
{
	if($quantity === "yes")
	{
		$quantity = 1;
	}
	else
	{
		$quantity = (int)$quantity;
	}

	if($quantity >= 1)
		return "{$item}:{$quantity}:{$cost},";
	else
		return "";
}

function ATU($item, $value)
{
	$value = urlencode($value);
	return "&{$item}={$value}";
}

if($_SERVER["REQUEST_METHOD"] == "POST") 
{
	$conn = Db::GetNewConnection();

	$emerald = "";
	$diamond = "";
	$ruby = "";
	$platinum = "";
	$gold = "";
	$silver = "";
	$ticket = "";
	$first_name = "";
	$last_name = "";
	$company = "";
	$email = "";
	$phone = "";
	$address = "";
	$state = "";
	$city = "";
	$zip = "";
	$paid = "";

	$adFull = "";
	$adHalf = "";
	$adQuarter = "";
	$adBiz = "";
	$adParton = "";

	foreach ($_POST as $key => $value) 
	{
		if(isset($$key)) 
		{
			$$key = $value;
		}
	}

	if($company == "") 
	{
		$company = "individual";
	}

	$paid = $paid === "yes" ? 1 : 0;
	$paid = !$paid;

	$emeraldCost = 10000;
	$diamondCost = 5000;
	$rubyCost = 3000;
	$platinumCost = 1000;
	$goldCost = 700;
	$silverCost = 350;
	$ticketCost = 35;
	$adFullCost = 150;
	$adHalfCost = 100;
	$adQuarterCost = 60;
	$adBizCost = 30;
	$adPartonCost = 20;

	$cost = 0;
	$cost += $emerald === "yes" ? $emeraldCost : 0;
	$cost += $diamond === "yes" ? $diamondCost : 0;
	$cost += $ruby === "yes" ? $rubyCost : 0;
	$cost += $platinum === "yes" ? $platinumCost : 0;
	$cost += $gold === "yes" ? $goldCost : 0;
	$cost += $silver === "yes" ? $silverCost : 0;

	$cost += $adFull === "yes" ? $adFullCost : 0;
	$cost += $adHalf === "yes" ? $adHalfCost : 0;
	$cost += $adQuarter === "yes" ? $adQuarterCost : 0;
	$cost += $adBiz === "yes" ? $adBizCost : 0;
	$cost += $adParton === "yes" ? $adPartonCost : 0;

	$cost += (int)$ticket * $ticketCost;

	$dataStr = "";
	$dataStr .= addItem(1, $emerald, $emeraldCost);
	$dataStr .= addItem(2, $diamond, $diamondCost);
	$dataStr .= addItem(3, $ruby, $rubyCost);
	$dataStr .= addItem(4, $platinum, $platinumCost);
	$dataStr .= addItem(5, $gold, $goldCost);
	$dataStr .= addItem(6, $silver, $silverCost);
	$dataStr .= addItem(7, $ticket, $ticketCost);

	$dataStr .= addItem(8, $adFull, $adFullCost);
	$dataStr .= addItem(9, $adHalf, $adHalfCost);
	$dataStr .= addItem(10, $adQuarter, $adQuarterCost);
	$dataStr .= addItem(11, $adBiz, $adBizCost);
	$dataStr .= addItem(12, $adParton, $adPartonCost);

	$dataStr = rtrim($dataStr, ",");


	$first_name = Db::EscapeString($first_name, $conn);
	$last_name = Db::EscapeString($last_name, $conn);
	$company = Db::EscapeString($company, $conn);
	$email = Db::EscapeString($email, $conn);
	$phone = Db::EscapeString($phone, $conn);
	$address = Db::EscapeString($address, $conn);
	$state = Db::EscapeString($state, $conn);
	$city = Db::EscapeString($city, $conn);
	$zip = Db::EscapeString($zip, $conn);
	$dataStrn = Db::EscapeString($dataStr, $conn);

	$AppLogin = "testapp.enablepoint.com";
	$AuthToken = "TGT-192-6soiicu2rYJtueIpgMY9LQ";

	$initiate_url = "https://paymentservices.ptcfe.intuit.com/paypage/ticket/create?AuthModel=desktop&AppLogin={$AppLogin}&AuthTicket={$AuthToken}&TxnType=Sale&Amount={$cost}";
	$render_url = "https://paymentservices.ptcfe.intuit.com/checkout/terminal?Ticket={0}&OpId={1}&action=checkout";

	if(true) // live
	{
		$AppLogin = "tastefest.enablepoint.com";
		$AuthToken = 'TGT-54-WOat80$Jhhl3wRy43VbbVw';

		$initiate_url = "https://paymentservices.intuit.com/paypage/ticket/create?AuthModel=desktop&AppLogin={$AppLogin}&AuthTicket={$AuthToken}&TxnType=Sale&Amount={$cost}";
		$render_url = "https://paymentservices.intuit.com/checkout/terminal?Ticket={0}&OpId={1}&action=checkout";
	}

	$initiate_url .= ATU("CustomerStreet", $address);
	$initiate_url .= ATU("CustomerCity", $city);
	$initiate_url .= ATU("CustomerState", $state);
	$initiate_url .= ATU("CustomerPostalCode", $zip);

	$stuff = array();

	$intret = file_get_contents($initiate_url);

	foreach (explode("\n", $intret) as $value) 
	{
		$parts = explode("=", $value, 2);
		if(count($parts) > 1)
			$stuff[$parts[0]] = rtrim($parts[1], "\r\n");
	}

	if(array_key_exists("OpId", $stuff))
	{
		$render_url = str_replace("{0}", urlencode($stuff["Ticket"]), $render_url);
		$render_url = str_replace("{1}", urlencode($stuff["OpId"]), $render_url);

		$event = Db::ExecuteNonQuery("INSERT INTO c_transaction (opid, data, status, first_name, last_name, company, email, address, city, state, zip, phone, paid) VALUES ('{$stuff["OpId"]}', '$dataStr', 0, '$first_name', '$last_name', '$company', '$email', '$address', '$city', '$state', '$zip', '$phone', '$paid')", $conn);

		if($paid)
		{
			redirect($render_url);
		}
		else
		{
			$context = getDefaultContext();
			echo $twig->render('intuit_landing_success.html', $context);
			Db::CloseConnection($conn);
			exit();
		}
	}
	
	Db::CloseConnection($conn);
}

$context = getDefaultContext();
echo $twig->render('register.html', $context);