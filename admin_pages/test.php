<?php

class Db
{
	public static function GetNewConnection($db_server, $db_user, $db_password, $db_database)
	{
		return new mysqli($db_server, $db_user, $db_password, $db_database);
	}
	public static function CloseConnection($conn)
	{
		$conn->close();
	}

	public static function CheckConnection($conn)
	{
		if($conn != null)
			return true;
		else
			return false;
	}

	public static function EscapeString($string, $conn)
	{
		return $conn->escape_string($string);
	}

	public static function ExecuteQuery($query, $conn)
	{
		$result = $conn->query($query);
		if($conn->errno)
			throw new Exception($conn->error, 1);

		$data = array();
		while($row = $result->fetch_assoc())
		{
			$data[] = $row;
		}

		$result->free();

		return $data;
	}
	public static function ExecuteNonQuery($query, $conn)
	{
		$conn->query($query);
		if($conn->errno)
			throw new Exception($conn->error, 1);
	}

	public static function ExecuteMultiNonQuery($query, $conn)
	{
		$conn->multi_query($query);
		if($conn->errno)
			throw new Exception($conn->error, 1);
	}

	public static function ExecuteFirst($query, $conn)
	{
		$result = $conn->query($query);
		if($conn->errno)
			throw new Exception($conn->error, 1);

		$data = null;
		if($row = $result->fetch_assoc())
		{
			$data = $row;
		}

		$result->free();

		return $data;
	}

	public static function GetLastInsertID($conn)
	{
		return $conn->insert_id;
	}
}

class BackupMaker
{
	public static function MakeBackup($db_server, $db_user, $db_password, $db_database)
	{
		$conn = Db::GetNewConnection($db_server, $db_user, $db_password, $db_database);

		$tables = Db::ExecuteQuery("SHOW TABLES;", $conn);

		$table_list = array();

		foreach ($tables as $key => $ntable) 
		{
			foreach ($ntable as $nkey => $table) 
			{
				$table_list[] = $table;
			}
		}

		$creates = array();

		foreach ($table_list as $table) 
		{
			$c = Db::ExecuteQuery("SHOW CREATE TABLE `{$table}`;", $conn);
			foreach ($c as $key => $value) 
			{
				$creates[] = $value["Create Table"] . ";";
			}
		}

		$creates = implode("\n\n", $creates);

		$data_backup = array();

		foreach ($table_list as $table) 
		{
			$data = Db::ExecuteQuery("SELECT * FROM `{$table}`", $conn);
			$headers = null;

			foreach ($data as $line) 
			{
				if($headers === null)
				{
					// fill headers
					$headers = array();
					foreach ($line as $key => $value) 
					{
						$headers[] = '`' . $key . '`';
					}
				}
				$_bak = "INSERT INTO `{$table}` (" . implode(", ", $headers) . ") VALUES (";
				$tmp = array();
				foreach ($line as $value) 
				{
					$tmp[] = "'" . Db::EscapeString($value, $conn) . "'";
				}
				$_bak .= implode(", ", $tmp) . ");";
				$data_backup[] = $_bak;
			}
		}

		Db::CloseConnection($conn);

		$backup = <<<EOD
-- Schema Backup


EOD;

		$backup .= $creates;

		$backup .= <<<EOD


-- Data Backup


EOD;

		$backup .= implode($data_backup, "\n");

		return $backup;
	}
}

echo BackupMaker::MakeBackup("127.0.0.1", "root", "", "newcms");