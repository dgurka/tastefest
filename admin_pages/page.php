<?php

require_once(BASE_DIR . "includes/admin_head.php");
require_once(BASE_DIR . "bootstrap.php");

$context = getDefaultContext();

$conn = Db::GetNewConnection();

if($_SERVER["REQUEST_METHOD"] == "POST")
{
	$title = Db::EscapeString(post("title"), $conn);
	$menukey = Db::EscapeString(post("menukey"), $conn);
	$linkoverride = Db::EscapeString(post("linkoverride"), $conn);
	$description = Db::EscapeString(post("description"), $conn);
	$content = Db::EscapeString(post("content"), $conn);

	if(isset($matches[1]))
	{
		$id = (int)$matches[1];

		$query = "UPDATE page SET title = '$title', menukey = '$menukey', linkoverride = '$linkoverride', description = '$description', content = '$content' WHERE ID = $id";

		Db::ExecuteNonQuery($query, $conn);
		redirect(URL_ROOT . "admin/page/" . $id . "/");
	}
	else
	{
		$query = "INSERT INTO page (title, menukey, linkoverride, description, content) VALUES ('$title', '$menukey', '$linkoverride', '$description', '$content')";

		Db::ExecuteNonQuery($query, $conn);
		$id = Db::GetLastInsertID($conn);
		redirect(URL_ROOT . "admin/page/" . $id . "/");
	}

	exit();
}

if(isset($matches[1]))
{
	$id = (int)$matches[1];
	$context["id"] = $id;
	$page = Db::ExecuteFirst("SELECT * FROM page WHERE ID = $id", $conn);
	$title = str_replace("\"", "&quot;", $page["title"]);
	$menukey = $page["menukey"];
	$linkoverride = $page["linkoverride"];
	$description = $page["description"];
	$content = $page["content"];
}
else
{
	$title = "";
	$menukey = -1;
	$linkoverride = "";
	$description = "";
	$content = "";
}

$menuselect = array();
$menuselect[] = "<option value='-1'>Not in menu</option>";
$menuitems = Db::ExecuteQuery("SELECT * FROM menu ORDER BY ID", $conn);
foreach ($menuitems as $value) 
{
	$mid = $value["ID"];
	$mname = $value["name"];
	$m = "<option value=\"$mid\"";
	if($mid == $menukey)
		$m .= " selected='selected'";
	$m .= ">$mname</option>";
	$menuselect[] = $m;
}

$context["title"] = $title;
$context["linkoverride"] = $linkoverride;
$context["description"] = $description;
$context["content"] = $content;
$context["menuselect"] = implode("", $menuselect);

echo $twig->render('page.html', $context);