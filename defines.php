<?php

// define constants, there are seperate sets for local and live

define("WEBSITE_TITLE", "Taste Fest");

define("BASE_DIR", dirname(__FILE__) . "/");
define("TEMPLATE_DIR", BASE_DIR . "templates/");
define("TEMPLATE_PACKAGE", "taste");
define("ADMIN_TEMPLATE_PACKAGE", "admin"); // use this in the 
define("DISPLAY_ERRORS", true);
define("ADMIN_EMAIL", "brianduggan16@gmail.com");
define("NO_REPLY_EMAIL", "no-reply@enablepoint.com");

$r = $_SERVER["PHP_SELF"];
$r = str_replace("index.php", "", $r);

if($r == "/")
{
	define("URI_BASE", "/");
	
	define("REQUEST_URI", ltrim($_SERVER["REQUEST_URI"], "/"));
}
else
{
	define("URI_BASE", str_replace("/", "\\/", $r));
	
	define("REQUEST_URI", preg_replace("/^" . URI_BASE . "/", "", $_SERVER["REQUEST_URI"]));
}

define("URL_ROOT", $r);

if($_SERVER["HTTP_HOST"] == "localhost")
{

	define("DB_SERVER", "127.0.0.1");
	define("DB_USER", "root");
	define("DB_PASSWORD", "");
	define("DB_DATABASE", "tastefest");
}
else
{

	define("DB_SERVER", "newtaste.db.5350222.hostedresource.com");
	define("DB_USER", "newtaste");
	define("DB_PASSWORD", "Taste817!");
	define("DB_DATABASE", "newtaste");
}

define("STATIC_ROOT", URL_ROOT . "templates/" . TEMPLATE_PACKAGE . "/"); // points to the web root of the template
define("IMAGE_ROOT", STATIC_ROOT . "images/");
define("CSS_ROOT", STATIC_ROOT . "css/");

// usage defines
define("EDITABLES_COUNT", 5); // if > 0 render editables in index.php context
define("USE_DROPDOWN", true); // if true render downdown in the context
define("MAIN_PAGE", "page/1/");
define("EVENT_CATEGORIES", "Entertainment,Community,Sports");
define("GENERAL_EVENT_NAME", "General Events");
define("USE_CACHE", false);
define("CACHE_DRIVER_ORDER", "APC,DB");
define("CAN_ADD_PAGES", false);
define("USE_DIRECTORY", false);

define("USE_GALLERY", false);
define("GALLERY_NAMES", "Gallery");

// app use constants
require_once("app_defines.php");